<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grid extends Model
{
    protected $fillable = [
        'title',
        'grid_data'
    ];

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order');
    }
}
