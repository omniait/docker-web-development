<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'name',
        'description',
        'value'
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
