<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    // array of mass assignable values
    protected $fillable = [
        'uuid',
        'nominative',
        'telephone',
        'notes'
    ];

    // A customer can have many orders
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
