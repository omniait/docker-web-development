<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // extremes included
    protected $fillable = [
        'start_date',
        'end_date'
    ];

    // An order is made by a customer
    public function customers()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    // An order has one rate
    public function rates()
    {
        return $this->belongsTo('App\Models\Rate');
    }

    // An order can belong to many grids (ex. cabin and tent)
    public function grids()
    {
        return $this->belongsToMany('App\Models\Grid');
    }
}
