<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Build a success response.
     *
     * @param string/array $data - Data received
     * @param int          $code - Success code
     *
     * @return Illuminate\Http\JsonResponse - HTTP response to the request
     */
    public function successResponse($data, int $code = Response::HTTP_OK)
    {
        return response($data, $code)->header('Content-Type', 'application/json');
    }

    /**
     * Build an error response.
     *
     * @param string/array $message - Error message
     * @param int          $code    - Any error code
     *
     * @return Illuminate\Http\JsonResponse - HTTP response to the request
     */
    public function errorResponse($message, int $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    /**
     * Build an error message.
     *
     * @param string/array $message - Error message
     * @param int          $code    - Any error code
     *
     * @return Illuminate\Http\JsonResponse - HTTP response to the request
     */
    public function errorMessage($message, int $code)
    {
        return response($message, $code)->header('Content-Type', 'application/json');
    }
}
