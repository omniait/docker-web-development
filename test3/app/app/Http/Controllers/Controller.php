<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Build a success response.
     *
     * @param string/array $data - Data received
     * @param int          $code - Success code
     *
     * @return Illuminate\Http\JsonResponse - HTTP response to the request
     */
    /*
    public function success($data, $code)
    {
        return response()->json(['data' => $data], $code);
    }
    */
    /**
     * Build an error response.
     *
     * @param string/array $message - Error message
     * @param int          $code    - Any error code
     *
     * @return Illuminate\Http\JsonResponse - HTTP response to the request
     */
    /*
    public function error($message, $code)
    {
        return response()->json(['message' => $message], $code);
    }
    */
}
