<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Models\Customer;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * * Get Request Methods
     * Return the list of all the customers.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        // get all customers
        $customers = Customer::all();
        foreach ($customers as $customer) {
            // decrypt sensible data
            $customer->telephone = decrypt($customer->telephone);
        }

        // return the result
        return $this->successResponse($customers, Response::HTTP_OK);
    }

    /**
     * Return a customer by its uuid.
     *
     * @param string $uuid - The customer uuid
     *
     * @return Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        // get the customer
        //$customer = Customer::where('uuid', '=', $uuid)->firstOrFail();
        $customer = Customer::findOrFail($uuid);
        // decrypt sensible data
        $customer->telephone = decrypt($customer->telephone);
        // return the result
        return $this->successResponse($customer, Response::HTTP_OK);
    }

    /**
     * * Post Request Method
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request - Post request with new data
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // specify validation rules
        $rules = [
            'nominative' => 'required|string|unique:customers|max:64',
            'telephone' => 'required|string|max:256',
            'other' => 'string'
        ];
        // validate rules
        $this->validate($request, $rules);
        // get data
        $data = $request->all();

        // encrypt sensible data
        $data['telephone'] = encrypt($data['telephone']);
        // make a uuid
        $data['uuid'] = Uuid::uuid4()->toString();
        // create new customer
        $customer = Customer::create($data);

        // return response()->json(['data' => $customer], 201);
        return $this->successResponse($customer, Response::HTTP_CREATED);
    }

    /**
     * * Put Request Method
     * Update an existing resource.
     *
     * @param \Illuminate\Http\Request $request - Put request with updated data
     * @param string                   $uuid    - The uuid of the customer resource to update
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $uuid)
    {
        // find customer
        $customer = Customer::findOrFail($uuid);
        // specify validation rules
        $rules = [
            'nominative' => 'string|unique:customers|max:64',
            'telephone' => 'string|max:256',
            'other' => 'string'
        ];
        // validate rules
        $this->validate($request, $rules);
        // get data
        $data = $request->all();

        if ($request->has('nominative')) {
            $customer->nominative = $request->nominative;
        }
        if ($request->has('telephone')) {
            $customer->telephone = encrypt($request->telephone);
        }
        if ($request->has('other')) {
            $customer->other = $request->other;
        }

        $customer->save();

        return $this->successResponse($customer, Response::HTTP_OK);
    }

    /**
     * * Delete Request Method
     * Destroy a customer resource.
     *
     * @param string $uuid - The uuid of the customer to delete
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $uuid)
    {
        $customer = Customer::findOrFail($uuid);

        $customer->delete();

        return $this->successResponse($customer, Response::HTTP_OK);
    }
}
