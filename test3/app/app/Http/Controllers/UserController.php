<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
// hash utility
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * * Get Request Methods
     * Return the list of all the users.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        // get all users
        $users = User::all();
        // return the result
        return $this->successResponse($users, Response::HTTP_OK);
    }

    /**
     * Return a user by its uuid.
     *
     * @param string $uuid - The user uuid
     *
     * @return Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        // get the user
        $user = User::findOrFail($uuid);
        // return the result
        return $this->successResponse($user, Response::HTTP_OK);
    }

    /**
     * * Post Request Method
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request - Post request with new data
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // specify validation rules
        $rules = [
            'name' => 'string|unique:users|max:64',
            'email' => 'required|email|unique:users|max:256',
            'password' => 'required|string'
        ];
        // validate rules
        $this->validate($request, $rules);
        // get data
        $data = $request->all();
        // by default can't be admin
        $data['is_admin'] = false;
        // make a uuid
        $data['uuid'] = Uuid::uuid4()->toString();
        // create new user
        $user = User::create($data);

        return $this->successResponse($user, Response::HTTP_CREATED);
    }

    /**
     * * Put Request Method
     * Update an existing resource.
     *
     * @param \Illuminate\Http\Request $request - Put request with updated data
     * @param string                   $uuid    - The uuid of the user resource to update
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $uuid)
    {
        // find user
        $user = User::findOrFail($uuid);
        // specify validation rules
        $rules = [
            'name' => 'string|unique:users|max:64',
            'email' => 'email|unique:users|max:256',
            'password' => 'string'
        ];
        // validate rules
        $this->validate($request, $rules);
        // get data
        $data = $request->all();

        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('email')) {
            $user->email = $request->telephone;
        }
        if ($request->has('password')) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return $this->successResponse($user, Response::HTTP_OK);
    }

    /**
     * * Delete Request Method
     * Destroy a user resource.
     *
     * @param string $uuid - The uuid of the user to delete
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $uuid)
    {
        $user = User::findOrFail($uuid);

        $user->delete();

        return $this->successResponse($user, Response::HTTP_OK);
    }
}
