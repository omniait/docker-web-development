<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$router->get('/', function () use ($router) {
    return $router->app->version();
});
*/

//* Basic models routes

//* Users routes
//* Get Methods
$router->get('/users', 'UserController@index');
$router->get('/users/{uuid}', 'UserController@show');
//* Post Method
$router->post('/users', 'UserController@store');
//* Put Method
$router->put('/users/{uuid}', 'UserController@update');
//* Delete Method
$router->delete('/users/{uuid}', 'UserController@destroy');

//* Customers routes
//* Get Methods
$router->get('/customers', 'CustomerController@index');
$router->get('/customers/{uuid}', 'CustomerController@show');
//* Post Method
$router->post('/customers', 'CustomerController@store');
//* Put Method
$router->put('/customers/{uuid}', 'CustomerController@update');
//* Delete Method
$router->delete('/customers/{uuid}', 'CustomerController@destroy');
