<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGridOrderPivotTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('grid_order', function (Blueprint $table) {
            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('grid_uuid', 36)->index();
            $table->foreign('grid_uuid')->references('uuid')->on('grids')->onDelete('cascade');

            $table->string('order_uuid', 36)->index();
            $table->foreign('order_uuid')->references('uuid')->on('orders')->onDelete('cascade');

            $table->primary(['grid_uuid', 'order_uuid']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('grid_order');
    }
}
