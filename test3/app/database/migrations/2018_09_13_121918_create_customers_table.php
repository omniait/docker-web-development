<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('uuid', 36);
            $table->string('nominative', 64)->unique();
            $table->string('telephone', 256)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
