<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            Schema::enableForeignKeyConstraints();

            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('uuid', 36);
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();

            $table->primary('uuid');

            $table->string('rates_uuid', 36)->index();
            $table->foreign('rates_uuid')->references('uuid')->on('rates')->onDelete('cascade');

            $table->string('customers_uuid', 36)->index();
            $table->foreign('customers_uuid')->references('uuid')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
