<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('uuid', 36);
            $table->string('name', 256)->nullable();
            $table->string('email', 128)->unique();
            // example password: $2a$04$CO8mflLEyy9XnsLbD7eCCOTnJ0eCO80pCNpMGNX7bJ8qpYrdSRqtC
            $table->string('password', 60);
            // $table->timestamp('email_verified_at')->nullable();
            $table->boolean('is_admin')->default(false);
            // $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
