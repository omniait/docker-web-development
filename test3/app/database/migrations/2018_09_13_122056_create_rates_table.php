<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('uuid', 36);
            $table->string('name', 32)->unique();
            $table->text('description')->nullable();
            $table->decimal('value', 8, 2);
            $table->timestamps();

            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
