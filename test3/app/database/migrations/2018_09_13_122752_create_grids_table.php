<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGridsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('grids', function (Blueprint $table) {
            // example uuid: 15406dc2-ee9c-41be-92b2-7f877cd01af5
            $table->string('uuid', 36);
            $table->string('title', 32)->unique();
            $table->longText('grid_data');
            $table->timestamps();

            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('grids');
    }
}
