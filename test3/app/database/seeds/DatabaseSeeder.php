<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        factory('App\Models\User', 20)->create();
        factory('App\Models\Customer', 20)->create();
        factory('App\Models\Rate', 20)->create();
        factory('App\Models\Grid', 20)->create();
        factory('App\Models\Order', 20)->create();
    }
}
