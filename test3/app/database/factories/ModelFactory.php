<?php

// uuid generator
use Ramsey\Uuid\Uuid;
// hash utility
use Illuminate\Support\Facades\Hash;
// get correct foreign keys
use App\Models\Customer;
use App\Models\Rate;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    //$hasher = app()->make('hash');

    return [
        'uuid' => Uuid::uuid4()->toString(),
        'name' => $faker->unique()->name,
        'email' => $faker->email,
        'password' => Hash::make($faker->word),
        'is_admin' => $faker->boolean
    ];
});

$factory->define(App\Models\Customer::class, function (Faker\Generator $faker) {
    return [
        'uuid' => Uuid::uuid4()->toString(),
        'nominative' => $faker->unique()->name,
        'telephone' => encrypt($faker->phoneNumber),
        'notes' => $faker->sentence(5)
    ];
});

$factory->define(App\Models\Rate::class, function (Faker\Generator $faker) {
    return [
        'uuid' => Uuid::uuid4()->toString(),
        'name' => $faker->unique()->word,
        'description' => $faker->sentence(2),
        'value' => $faker->randomFloat(2, 0, 0, 9999.99)
    ];
});

function randomMatrix(Faker\Generator $faker)
{
    $array = [];
    $nRows = $faker->numberBetween(2, 20);
    $nCols = $faker->numberBetween(2, 10);

    for ($i = 0; $i < $nRows; $i++) {
        array_push($array, []);
        for ($j = 0; $j < $nCols; $j++) {
            array_push($array[$i], $faker->word());
        }
    }

    return $array;
}

$factory->define(App\Models\Grid::class, function (Faker\Generator $faker) {
    return [
        'uuid' => Uuid::uuid4()->toString(),
        'title' => $faker->unique()->word(),
        'grid_data' => json_encode([
            'cellType' => $faker->randomElement(['text', 'icons']),
            'selectedIconSet' => $faker->randomElement(['i8color', 'i8iceCream', 'i8metro', 'i8office', 'i8uv', 'i8w10', 'iso']),
            'content' => randomMatrix($faker),
        ])
        //'gridData' => $faker->text(),
    ];
});

$factory->define(App\Models\Order::class, function (Faker\Generator $faker) {
    // get customers and rates
    $customers = Customer::all()->pluck('uuid')->toArray();
    $rates = Rate::all()->pluck('uuid')->toArray();

    //var_dump($customers);

    return [
        'uuid' => Uuid::uuid4()->toString(),
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'customers_uuid' => $faker->randomElement($customers),
        'rates_uuid' => $faker->randomElement($rates)
    ];
});
